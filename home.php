<?php 
// Template Name: Página de lista de lobinhos
?>

<?php get_header();?>

    <div class="container_s">
        <div>
            <div class="search_bar">
                <img src="<?php echo get_stylesheet_directory_uri() ?>/images/icon_dog_paw.ico" alt="paw dog icon">
                <input type="text">
            </div>

            <a href="cadastrarLobinho.html"><button class="button_search" type="submit">+ Lobo</button></a>
        </div>
            
        
        <div class="check_lobinho">
            <input type="checkbox">
            <label for="check">Ver Lobinhos Adotados</label>
        </div>
            
    </div>
    
    <div id="todos_os_lobos">
        <?php 
            // Define our WP Query Parameters
            $the_query = new WP_Query( 'posts_per_page=4' );
        ?>

        <!-- Iniciando Variável -->
        <?php $numero = 0; ?>

        <?php
            while( $the_query->have_posts()) : $the_query -> the_post();
        ?>

        <!-- Primeiro caso da variável -->
        <?php if($numero % 2 == 0){ ?>

        <div class="_lobo">
            <figure class="imgbox1">
                <?php if( get_field('lobo_foto') ): ?>
                    <img src="<?php the_field('lobo_foto'); ?>">
                <?php endif; ?>
            </figure>
            <div class="coluna1">
                <h3><?php the_field('lobo_titulo'); ?></h3>
                <p>Idade: <?php the_field('lobo_idade'); ?> anos</p>
                <p><?php the_field('lobo_descricao'); ?></p>
                <a href="adocaoLobinho.html"><button class="button_search">Adotar</button></a>
            </div>
        </div>

        <!-- Segundo caso da variável -->
        <?php }
        else{ ?>

        <div class="_lobo">
            <div class="coluna2">
                <h3><?php the_field('lobo_titulo'); ?></h3>
                <p>Idade: <?php the_field('lobo_idade'); ?> anos</p>
                <p><?php the_field('lobo_descricao'); ?></p>
                <a href="adocaoLobinho.html"><button class="button_search">Adotar</button></a>
            </div>
            <figure class="imgbox2">
                <?php if( get_field('lobo_foto') ): ?>
                    <img src="<?php the_field('lobo_foto'); ?>">
                <?php endif; ?>
            </figure>
        </div>
        
        <!-- Valor que altera valores da variável -->
        <?php }
        $numero++;?>

        <?php
        endwhile;
        wp_reset_postdata();
        ?>

        <?php echo paginate_links()?>
    </div>
    
    <?php get_footer();?>