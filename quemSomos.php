<?php 
// Template Name: Página de Quem Somos.
?>

<?php get_header();?>

    <section class="quem_somos">
        <h1><?php the_title();?></h1>
        <p><?php the_content();?></p>
    </section>


<?php get_footer();?>