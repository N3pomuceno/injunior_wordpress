<?php 
// Template Name: Página Inicial
?>

<?php get_header() ?>
<main>
    <section class="sec_lobo">
        <h1><?php the_field('titulo_inicial'); ?></h1>
        <div id="linha_lobo"></div>
        <p><?php the_field('descricao_inicial'); ?></p>
    </section>

    <section class="sec_sobre">
        <h2><?php the_field('titulo_sobre'); ?></h2>
        <p><?php the_field('descricao_sobre'); ?></p>
    </section>
    
    <section class="sec_valores">
        <h2><?php the_field('valores_titulo_valor'); ?></h2>
        <div class="sub_sec_valores">
            <div class ="card" id="protecao">
                <div>
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/./images/values_life-insurance.svg">
                </div>
                <h3><?php the_field('valores_titulo_valor_1'); ?></h3>
                <p><?php the_field('valores_descricao_valor_1'); ?></p>
            </div>
            <div class ="card" id="carinho">
                <div>
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/./images/values_care.svg">
                </div>
                <h3><?php the_field('valores_titulo_valor_2'); ?></h3>
                <p><?php the_field('valores_descricao_valor_2'); ?></p>
            </div>
            <div class ="card" id="companherismo">
                <div>
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/./images/values_Group.svg">
                </div>
                <h3><?php the_field('valores_titulo_valor_3'); ?></h3>
                <p><?php the_field('valores_descricao_valor_3'); ?></p>
            </div>
            <div class ="card" id="resgate">
                <div>
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/./images/values_RescueDog.svg">
                </div>
                <h3><?php the_field('valores_titulo_valor_4'); ?></h3>
                <p><?php the_field('valores_descricao_valor_4'); ?></p>
            </div>
        </div>
    </section>

    <!--Exemplos de lobinhos-->
    <section class="sec_exemplos">
        <h2>Lobos Exemplo</h2>
        <?php 
            // Define our WP Query Parameters
            $the_query = new WP_Query( 'posts_per_page=2' );
        ?>

        <!-- Iniciando Variável -->
        <?php $numero = 0; ?>

        <?php
            while( $the_query->have_posts()) : $the_query -> the_post();
        ?>

        <!-- Primeiro caso da variável -->
        <?php if($numero % 2 == 0){ ?>

        <div class="_lobo">
            <figure class="imgbox1">
                <?php if( get_field('lobo_foto') ): ?>
                    <img src="<?php the_field('lobo_foto'); ?>">
                <?php endif; ?>
            </figure>
            <div class="coluna1">
                <h3><?php the_field('lobo_titulo'); ?></h3>
                <p>Idade: <?php the_field('lobo_idade'); ?> anos</p>
                <p><?php the_field('lobo_descricao'); ?></p>
            </div>
        </div>

        <!-- Segundo caso da variável -->
        <?php }
        else{ ?>

        <div class="_lobo">
            <div class="coluna2">
                <h3><?php the_field('lobo_titulo'); ?></h3>
                <p>Idade: <?php the_field('lobo_idade'); ?> anos</p>
                <p><?php the_field('lobo_descricao'); ?></p>
            </div>
            <figure class="imgbox2">
                <?php if( get_field('lobo_foto') ): ?>
                    <img src="<?php the_field('lobo_foto'); ?>">
                <?php endif; ?>
            </figure>
        </div>
        
        <!-- Valor que altera valores da variável -->
        <?php }
        $numero++;?>

        <?php
        endwhile;
        wp_reset_postdata();
        ?>
    </section>
</main>
<?php get_footer();?>