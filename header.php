<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/style.css">
    <title>Home Page</title>
    <?php wp_head();?>
</head>
<body>
    <header>
        <nav>
            <ul>
                <?php
                    $args = array(
                        'menu'=>'navegacao',
                        'container' => true
                    );
                    wp_nav_menu($args);
                ?>
            </ul>
        </nav>
        
        <!-- <a href="home.php">Nossos Lobinhos</a>
        <a href="index.php"><img src="<?php //echo get_stylesheet_directory_uri() ?>/./images/header_logo.png"></a>
        <a href="quemSomos.php">Quem Somos</a> -->
    </header>
